# Pathfinder

A very WIP C++ game engine.
Current Features:
* Phong Shading
* Can handle in a 1 ly^3 area to a resolution of 1 nm.
* Objects can have nested coordinate systems, accumulating rotations and all.
