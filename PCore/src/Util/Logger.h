#pragma once

#include <vector>
#include <chrono>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>

namespace Logger {
    enum LoggerLevel {
        ERROR,
        WARN,
        INFO,
        DEBUG
    };

    std::string loggerLevelToString(LoggerLevel level);

    void log(const LoggerLevel level, const std::string &message);
}