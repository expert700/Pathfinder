#pragma once

#include <csignal>
#include <entity/registry.hpp>


class System {
public:
	virtual void init(entt::DefaultRegistry &registry) {}
	virtual void process(double delta, entt::DefaultRegistry &registry) {}

	double systemDelta = 0.06f;
	std::string systemName = "BaseSystem";
};
