#pragma once

#include <unordered_map>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

#include <entity/registry.hpp>

#include "../Component/Transform.h"
#include "../System.h"

class Physics: public System {
public:
    void init(entt::DefaultRegistry &registry) override;
    void process(double delta, entt::DefaultRegistry &registry) override;
};
