#include <iostream>
#include "Physics.h"

void Physics::init(entt::DefaultRegistry &registry) {
    systemName = "Physics";
}

void Physics::process(double delta, entt::DefaultRegistry &registry) {
    auto view = registry.view<Transform>();
    for (auto entity : view) {
        auto &transform = view.get(entity);
        transform.position.x += static_cast<long long>(transform.velocity.x * delta);
        transform.position.y += static_cast<long long>(transform.velocity.y * delta);
        transform.position.z += static_cast<long long>(transform.velocity.z * delta);
    }
}
