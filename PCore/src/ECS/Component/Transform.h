#pragma once

#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <entt.hpp>

struct Transform {
    entt::DefaultRegistry::entity_type parent = 0;

	glm::quat rotation = glm::quat(1, 0, 0, 0);
	glm::i64vec3 position;
	glm::i64vec3 velocity;
};
