#pragma once

#include <array>
#include <iostream>

#include <glm/glm.hpp>

//TODO; The buffers in this really need to be cleaned up! Find a solution.
struct StaticMesh {
    unsigned int vaoID;
    unsigned int vertexCount;
    std::string model;
    std::string shader;
	std::array<unsigned int, 4> vboIDs;
	float scale = 1.0;

	StaticMesh(const StaticMesh& staticMesh) {
		this->vaoID = staticMesh.vaoID;
		this->vertexCount = staticMesh.vertexCount;
		this->model = staticMesh.model;
		this->shader = staticMesh.shader;
		this->vboIDs = staticMesh.vboIDs;
		this->scale = staticMesh.scale;
	}

	StaticMesh() = default;
};
