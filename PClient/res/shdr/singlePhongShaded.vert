#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

uniform mat4 mvp;
uniform mat4 model;

out vec3 Normal;
out vec3 FragPos;

void main() {
    gl_Position = mvp * vec4(aPos, 1.0);

    // Note: Normals will need to be fixed if the model matrix has had
    // a non-uniform transformation applied to it.
    Normal = aNormal;
    FragPos = vec3(model * vec4(aPos, 1.0));
}