#version 330 core

out vec4 fragColor;

in vec3 texCoords;

uniform samplerCube cube;

void main() {
    fragColor = texture(cube, texCoords);
}