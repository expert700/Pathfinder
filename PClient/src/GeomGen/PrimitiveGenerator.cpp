#include <utility>
#include <Util/Logger.h>

#include "PrimitiveGenerator.h"

void PrimitiveGenerator::loadModel(std::string name, StaticMesh& staticMesh) {
    loadModel(&staticMesh, std::move(name));
}

void PrimitiveGenerator::loadModel(StaticMesh* smc, std::string name) {
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile("res/obj/" + name + ".obj", aiProcess_Triangulate );
    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        Logger::log(Logger::WARN, std::string("Assimp Failure : ") + importer.GetErrorString());
    }

    std::vector<float> tempVertices;
    std::vector<float> tempNormals;

    std::vector<float> vertices;
    std::vector<float> normals;


    for (int i = 0; i < scene->mNumMeshes; i++) {
        aiMesh* mesh = scene->mMeshes[i];

        for (int j = 0; j < mesh->mNumVertices; j++) {
            tempVertices.push_back(mesh->mVertices[j].x);
            tempVertices.push_back(mesh->mVertices[j].y);
            tempVertices.push_back(mesh->mVertices[j].z);

            if (mesh->HasNormals()) {
                tempNormals.push_back(mesh->mNormals[j].x);
                tempNormals.push_back(mesh->mNormals[j].y);
                tempNormals.push_back(mesh->mNormals[j].z);
            }
        }

        for (unsigned int j = 0; j < mesh->mNumFaces; j++) {
            aiFace face = mesh->mFaces[j];
            for (unsigned int k = 0; k < face.mNumIndices; k++) {
                int index = face.mIndices[k];

                vertices.push_back(tempVertices[index * 3]);
                vertices.push_back(tempVertices[index * 3 + 1]);
                vertices.push_back(tempVertices[index * 3 + 2]);

                if (mesh->HasNormals()) {
                    normals.push_back(tempNormals[index * 3]);
                    normals.push_back(tempNormals[index * 3 + 1]);
                    normals.push_back(tempNormals[index * 3 + 2]);
                } else {
                    normals.insert(normals.end(), {0.0f, 0.0f, 0.0f});
                }
            }
        }
    }

    glGenVertexArrays(1, & smc->vaoID);
    glBindVertexArray(smc->vaoID);

    smc->vertexCount = static_cast<unsigned int>(vertices.size());

    unsigned int vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices.front(), GL_STATIC_DRAW);

    unsigned int normalBuffer;
    glGenBuffers(1, &normalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals.front(), GL_STATIC_DRAW);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    smc->vboIDs.at(0) = vertexBuffer;
    smc->vboIDs.at(1) = normalBuffer;

}