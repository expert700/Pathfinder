#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <boost/algorithm/string.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "ECS/Component/StaticMesh.h"

class PrimitiveGenerator {
public:
	static void loadModel(std::string name, StaticMesh& staticMesh);
    static void loadModel(StaticMesh* smc, std::string name);
};
