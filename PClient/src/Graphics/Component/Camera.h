#pragma once

#include <glm/glm.hpp>

struct Camera {
    glm::mat4 projection;

	glm::vec3 right;
	glm::vec3 up;
    glm::vec3 forward;
};
