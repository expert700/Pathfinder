#pragma once

#include <memory>

#include "../ShaderDescriptor.h"

struct Material {
    std::shared_ptr<ShaderDescriptor> shader;

    std::map<GLenum, GLuint> textures;
};