#include <Util/Logger.h>
#include "ShaderRegistry.h"

ShaderRegistry::~ShaderRegistry() = default;

GLuint ShaderRegistry::getShader(std::string shader) {
    if (shaders.find(shader) != shaders.end()) return shaders.at(shader);

    GLuint glShaderID = compileShader(shader);
    shaders.insert(std::make_pair(shader, glShaderID));
    return glShaderID;
}

GLuint ShaderRegistry::compileShader(std::string shader) {
    GLuint vertexID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    
    std::string vertexData;
    std::string vertexPath = "res/shdr/" + shader + ".vert";
    std::ifstream vertexStream(vertexPath, std::ios::in);
    if (vertexStream.is_open()) {
        std::stringstream sstr;
        sstr << vertexStream.rdbuf();
        vertexData = sstr.str();
        vertexStream.close();
    } else {
        Logger::log(Logger::WARN, std::string("Could not open shader ") + vertexPath);
        return 0;
    }
    
    std::string fragmentData;
    std::string fragmentPath = "res/shdr/" + shader + ".frag";
    std::ifstream fragmentStream(fragmentPath, std::ios::in);
    if (fragmentStream.is_open()) {
        std::stringstream sstr;
        sstr << fragmentStream.rdbuf();
        fragmentData = sstr.str();
        fragmentStream.close();
    } else {
        Logger::log(Logger::WARN, std::string("Could not open shader ") + fragmentPath);
        return 0;
    }
    
    GLint result = GL_FALSE;
    int infoLogLength;

    Logger::log(Logger::INFO, std::string("Compiling shader : ") + vertexPath);
    const char* vertexSrcPtr = vertexData.c_str();
    glShaderSource(vertexID, 1, &vertexSrcPtr, nullptr);
    glCompileShader(vertexID);
    
    glGetShaderiv(vertexID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(vertexID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0) {
        std::vector<char> vertexErrMsg(infoLogLength + 1);
        glGetShaderInfoLog(vertexID, infoLogLength, nullptr, &vertexErrMsg[0]);
        Logger::log(Logger::WARN, std::string(static_cast<char *>(&vertexErrMsg[0])));
        return 0;
    }

    Logger::log(Logger::INFO, std::string("Compiling shader : ") + fragmentPath);
    const char* fragmentSrcPtr = fragmentData.c_str();
    glShaderSource(fragmentID, 1, &fragmentSrcPtr, nullptr);
    glCompileShader(fragmentID);
    
    glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(fragmentID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0) {
        std::vector<char> fragmentErrMsg(infoLogLength + 1);
        glGetShaderInfoLog(fragmentID, infoLogLength, nullptr, &fragmentErrMsg[0]);
        Logger::log(Logger::WARN, std::string(static_cast<char *>(&fragmentErrMsg[0])));
        return 0;
    }

    Logger::log(Logger::INFO, std::string("Linking program : ") + shader);
    GLuint programID = glCreateProgram();
    glAttachShader(programID, vertexID);
    glAttachShader(programID, fragmentID);
    glLinkProgram(programID);
    
    glGetProgramiv(programID, GL_LINK_STATUS, &result);
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0) {
        std::vector<char> programErrMsg(infoLogLength + 1);
        glGetProgramInfoLog(programID, infoLogLength, nullptr, &programErrMsg[0]);
        Logger::log(Logger::WARN, std::string(static_cast<char *>(&programErrMsg[0])));
        return 0;
    }
    
    glDetachShader(programID, vertexID);
    glDetachShader(programID, fragmentID);
    
    glDeleteShader(vertexID);
    glDeleteShader(fragmentID);
    
    return programID;
}

std::shared_ptr<ShaderDescriptor> ShaderRegistry::getShaderDescriptor(std::string shader) {
    if (shaderDescriptors.find(shader) != shaderDescriptors.end()) return shaderDescriptors.at(shader);
    GLuint shaderID = getShader(shader);
    shaderDescriptors.insert(std::make_pair(shader, std::make_shared<ShaderDescriptor>()));
    shaderDescriptors.at(shader)->shaderID = shaderID;
    return shaderDescriptors.at(shader);
}
