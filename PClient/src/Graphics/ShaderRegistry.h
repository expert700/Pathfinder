#pragma once

#include <unordered_map>
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <vector>

#include <GL/glew.h>

#include "ShaderDescriptor.h"

class ShaderRegistry {
public:
    ~ShaderRegistry();

    GLuint getShader(std::string shader);

    std::shared_ptr<ShaderDescriptor> getShaderDescriptor(std::string shader);
private:
    // Free performance boost down the road from mapping shaders to their integer id instead of a string. String lookups are slow.
    std::unordered_map<std::string, GLuint> shaders;
    std::unordered_map<std::string, std::shared_ptr<ShaderDescriptor>> shaderDescriptors;

    GLuint compileShader(std::string shader);
};
