#pragma once

#include <vector>
#include <map>

#include <glm/glm.hpp>
#include <GL/glew.h>

struct ShaderDescriptor {
    std::map<std::string, int> uniformIntegers;
    std::map<std::string, float> uniformFloats;
    std::map<std::string, glm::vec3> uniformVectors;

    GLuint shaderID;
    GLenum depthFunc = GL_LESS;

    unsigned char attribArrays = 1;

    bool passMVP = true;
    bool passModel = false;
    bool passProjection = false;
    bool passView = false;
    bool passLights = false;
};


