#include "Renderer.h"

void frameBufferSizeCallback(GLFWwindow *window, int width, int height) {
	glViewport(0, 0, width, height);
}

void Renderer::init(entt::DefaultRegistry &registry) {
    systemName = "Render";

	glfwSetFramebufferSizeCallback(Pathfinder::getInstance().getWindow(), &frameBufferSizeCallback);

	// Setup OpenGL flags.
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Compile shaders and setup descriptors.
    std::shared_ptr<ShaderDescriptor> singlePhongShaded = shaderRegistry.getShaderDescriptor("singlePhongShaded");
    singlePhongShaded->attribArrays = 2;
    singlePhongShaded->passModel = true;
    singlePhongShaded->passLights = true;
    singlePhongShaded->uniformVectors.insert(std::make_pair("material.ambient", glm::vec3(0.19225, 0.19225, 0.19225)));
    singlePhongShaded->uniformVectors.insert(std::make_pair("material.diffuse", glm::vec3(0.50754, 0.50754, 0.50754)));
    singlePhongShaded->uniformVectors.insert(std::make_pair("material.specular", glm::vec3(0.508273, 0.508273, 0.508273)));
    singlePhongShaded->uniformFloats.insert(std::make_pair("material.shininess", 32.0f));

    std::shared_ptr<ShaderDescriptor> cubeMapped = shaderRegistry.getShaderDescriptor("cubeMapped");
    cubeMapped->passMVP = false;
    cubeMapped->passView = true;
    cubeMapped->passProjection = true;
    cubeMapped->depthFunc = GL_LEQUAL;
    cubeMapped->uniformIntegers.insert(std::make_pair("cube", 0));

    std::shared_ptr<ShaderDescriptor> light = shaderRegistry.getShaderDescriptor("light");

    textShader = shaderRegistry.getShader("text");

    // Load spaceship.
    spaceship = registry.create();
    auto& spaceshipSM = registry.assign<StaticMesh>(spaceship);
    PrimitiveGenerator::loadModel("Spaceship", spaceshipSM);

    auto& spaceshipTransform = registry.assign<Transform>(spaceship);
    spaceshipTransform.position = glm::i64vec3(0, 0, 0);

    auto& spaceshipMaterial = registry.assign<Material>(spaceship);
    spaceshipMaterial.shader = singlePhongShaded;

    // Load skybox.
    std::vector<std::string> faces {
        "res/img/skybox/right.jpg",
        "res/img/skybox/left.jpg",
        "res/img/skybox/top.jpg",
        "res/img/skybox/bottom.jpg",
        "res/img/skybox/front.jpg",
        "res/img/skybox/back.jpg"
    };

    GLuint cubeMapTexture;
    glGenTextures(1, &cubeMapTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);

    int texWidth, texHeight, nrChannels;
    for (unsigned int i = 0; i < faces.size(); i++) {
        unsigned char *data = stbi_load(faces[i].c_str(), &texWidth, &texHeight, &nrChannels, 0);
        if (data) {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        } else {
            Logger::log(Logger::WARN, std::string("Failure loading skybox texture at : ") + faces[i]);
            stbi_image_free(data);
        }
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    auto skybox = registry.create();
    auto& skyboxSM = registry.assign<StaticMesh>(skybox);
    PrimitiveGenerator::loadModel("Skybox", skyboxSM);

    auto& skyboxTransform = registry.assign<Transform>(skybox);
    skyboxTransform.position = glm::vec3(0, 0, 0);

    auto& skyboxMaterial = registry.assign<Material>(skybox);
    skyboxMaterial.shader = cubeMapped;
    skyboxMaterial.textures.insert(std::make_pair(GL_TEXTURE_CUBE_MAP, cubeMapTexture));

    // Load light bulb.
    lightSphere = registry.create();

    auto& lightSM = registry.assign<StaticMesh>(lightSphere);
    PrimitiveGenerator::loadModel("Bulb", lightSM);

    auto& lightTransform = registry.assign<Transform>(lightSphere);
    lightTransform.parent = spaceship;
    lightTransform.position = glm::i64vec3(10000, 0, 10000);

    auto& lightMaterial = registry.assign<Material>(lightSphere);
    lightMaterial.shader = light;

    // Load spaceship 2.
    spaceship2 = registry.create();
    auto& spaceshipSM2 = registry.assign<StaticMesh>(spaceship2);
    PrimitiveGenerator::loadModel("Spaceship", spaceshipSM2);

    auto& spaceship2Transform = registry.assign<Transform>(spaceship2);
    spaceship2Transform.position = glm::i64vec3(0, 10000, 0);
    spaceship2Transform.parent = lightSphere;

    auto& spaceship2Material = registry.assign<Material>(spaceship2);
    spaceship2Material.shader = singlePhongShaded;

    // Load astero.
    auto astero = registry.create();
    auto& asteroSM = registry.assign<StaticMesh>(astero);
    PrimitiveGenerator::loadModel("Astero", asteroSM);

    auto& asteroTransform = registry.assign<Transform>(astero);
    asteroTransform.position = glm::i64vec3(2000000, 0, 100000);

    auto& asteroMaterial = registry.assign<Material>(astero);
    asteroMaterial.shader = singlePhongShaded;


    // Load player entity.
    int width, height;
    glfwGetFramebufferSize(Pathfinder::getInstance().getWindow(), &width, &height);
    
	auto playerEntity = Pathfinder::getInstance().getPlayerEntity();

    auto& cam = registry.assign<Camera>(playerEntity);
    cam.projection = glm::perspective(glm::radians(60.0f), (float) width / height, 0.1f, 2500.0f);
	cam.right = glm::vec3(1, 0, 0);
	cam.up = glm::vec3(0, 1, 0);

    auto& transform = registry.assign<Transform>(playerEntity);
    transform.parent = spaceship;
    transform.position = glm::i64vec3(5000, 0, 0);

    // Setup FreeType.
    if (FT_Init_FreeType(&ft)) {
        Logger::log(Logger::WARN, "Could not initialize FreeType.");
    }
    std::string font = "res/font/CaviarDreams_Bold.ttf";
    if (FT_New_Face(ft, font.c_str(), 0, &face)) {
        Logger::log(Logger::WARN, std::string("Failure loading font at :  ") + font);
    }

    FT_Set_Pixel_Sizes(face, 0, 32);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    for (GLubyte c = 0; c < 128; c++)
    {
        // Load character glyph
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            Logger::log(Logger::WARN, std::string("Failure loading glyph : ") + (char) c);
            continue;
        }
        // Generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RED,
                face->glyph->bitmap.width,
                face->glyph->bitmap.rows,
                0,
                GL_RED,
                GL_UNSIGNED_BYTE,
                face->glyph->bitmap.buffer
        );
        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // Now store character for later use
        Character character = {
                texture,
                glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                static_cast<GLuint>(face->glyph->advance.x)
        };
        characters.insert(std::pair<GLchar, Character>(c, character));
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    // Destroy FreeType once we're finished
    FT_Done_Face(face);
    FT_Done_FreeType(ft);

    glGenVertexArrays(1, &textVAO);
    glGenBuffers(1, &textVBO);
    glBindVertexArray(textVAO);
    glBindBuffer(GL_ARRAY_BUFFER, textVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, nullptr, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Renderer::process(double delta, entt::DefaultRegistry &registry) {
    static unsigned long long frames;
    frames++;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	auto playerEntity = Pathfinder::getInstance().getPlayerEntity();
    auto& playerCamera = registry.get<Camera>(playerEntity);
    auto& playerTransform = registry.get<Transform>(playerEntity);
	glm::mat4 viewMatrix = glm::lookAt(glm::vec3(0, 0, 0), playerCamera.forward, playerCamera.up);

	static double accum;
	accum += delta;

    //registry.get<Transform>(spaceship).position = glm::i64vec3(10000 + (float) sin(accum) * 25000, 2500, -12500 + (float) cos(accum) * 25000);
    //registry.get<Transform>(lightSphere).position = glm::i64vec3(10000 + (float) sin(accum) * 25000, 2500, -12500 + (float) cos(accum) * 25000);

    registry.get<Transform>(spaceship).rotation = glm::rotate(registry.get<Transform>(spaceship).rotation, 0.005f, glm::vec3(0, 0, 1));
    registry.get<Transform>(lightSphere).rotation = glm::rotate(registry.get<Transform>(lightSphere).rotation, 0.005f, glm::vec3(0, 1, 0));

	auto view = registry.view<StaticMesh, Transform, Material>();

	// Calculate world relative player position and rotation for later use.
    glm::i64vec3 netPlayerPosition = playerTransform.position;
    glm::quat netPlayerRotation = glm::quat(1, 0, 0, 0);
    entt::DefaultRegistry::entity_type currEntity = playerTransform.parent;
    while (currEntity != 0) {
        auto& currTransform = view.get<Transform>(currEntity);
        netPlayerPosition += currTransform.position;
        netPlayerRotation = currTransform.rotation * netPlayerRotation;
        currEntity = currTransform.parent;
    }

    //TODO; Less hacky lightPos.
    glm::i64vec3 netLightPosition(0, 0, 0);
	for (auto entity : view) {
        auto& objectStaticMesh = view.get<StaticMesh>(entity);
        auto& objectTransform = view.get<Transform>(entity);
        auto& objectMaterial = view.get<Material>(entity);

        glUseProgram(objectMaterial.shader->shaderID);
        glDepthFunc(objectMaterial.shader->depthFunc);
        if (objectMaterial.shader->passModel || objectMaterial.shader->passMVP) {
            glm::i64vec3 netObjectPosition = objectTransform.position;
            glm::quat netObjectRotation = objectTransform.rotation;
            currEntity = objectTransform.parent;
            while (currEntity != 0) {
                auto& currTransform = view.get<Transform>(currEntity);
                netObjectPosition = glm::toMat4(currTransform.rotation) * glm::vec4(netObjectPosition, 1.0);
                netObjectPosition += currTransform.position;
                netObjectRotation = currTransform.rotation * netObjectRotation;
                currEntity = currTransform.parent;
            }


            netObjectPosition = glm::toMat4(glm::inverse(netPlayerRotation)) * glm::vec4(netObjectPosition, 1.0);
            netObjectRotation = glm::inverse(netPlayerRotation) * netObjectRotation;

            glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(netObjectPosition - netPlayerPosition) / 1000.0f);
            model = glm::scale(model, glm::vec3(objectStaticMesh.scale));
            model *= glm::toMat4(netObjectRotation);
            if (objectMaterial.shader->passModel) {
                GLint modelLocation = getUniformLocation(objectMaterial.shader->shaderID, "model");
                glUniformMatrix4fv(modelLocation, 1, GL_FALSE, &model[0][0]);
            }
            if (objectMaterial.shader->passMVP) {
                glm::mat4 mvp = playerCamera.projection * viewMatrix * model;
                GLint mvpLocation = getUniformLocation(objectMaterial.shader->shaderID, "mvp");
                glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, &mvp[0][0]);
            }
            if (entity == lightSphere) {
                netLightPosition = netObjectPosition;
            }
        }

        if (objectMaterial.shader->passView) {
            GLint viewLocation = getUniformLocation(objectMaterial.shader->shaderID, "view");
            //TODO; This will probably break things at a later date.
            glm::mat4 viewToPass = viewMatrix * glm::toMat4(glm::inverse(netPlayerRotation));
            glUniformMatrix4fv(viewLocation, 1, GL_FALSE, &viewToPass[0][0]);
        }

        if (objectMaterial.shader->passProjection) {
            GLint projectionLocation = getUniformLocation(objectMaterial.shader->shaderID, "projection");
            glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, &playerCamera.projection[0][0]);
        }

        if (objectMaterial.shader->passLights) {
            GLint lightPositionLocation = getUniformLocation(objectMaterial.shader->shaderID, "light.position");
            GLint lightAmbientLocation = getUniformLocation(objectMaterial.shader->shaderID, "light.ambient");
            GLint lightDiffuseLocation = getUniformLocation(objectMaterial.shader->shaderID, "light.diffuse");
            GLint lightSpecularLocation = getUniformLocation(objectMaterial.shader->shaderID, "light.specular");

            //TODO; Figure out why light is wonky on Astero.
            glm::vec3 lightPos = glm::vec3(netLightPosition - netPlayerPosition) / 1000.0f;
            glUniform3f(lightPositionLocation, lightPos.x, lightPos.y, lightPos.z);
            glUniform3f(lightAmbientLocation, 0.2f, 0.2f, 0.2f);
            glUniform3f(lightDiffuseLocation, 0.5f, 0.5f, 0.5f);
            glUniform3f(lightSpecularLocation, 1.0f, 1.0f, 1.0f);
        }

        if (!objectMaterial.shader->uniformIntegers.empty()) {
            std::map<std::string, int>::iterator iterator;
            for (iterator = objectMaterial.shader->uniformIntegers.begin();
            iterator != objectMaterial.shader->uniformIntegers.end(); iterator++) {
                GLint integerLocation = getUniformLocation(objectMaterial.shader->shaderID, iterator->first);
                glUniform1i(integerLocation, iterator->second);
            }

        }

        if (!objectMaterial.shader->uniformFloats.empty()) {
            std::map<std::string, float>::iterator iterator;
            for (iterator = objectMaterial.shader->uniformFloats.begin();
            iterator != objectMaterial.shader->uniformFloats.end(); iterator++) {
                GLint floatLocation = getUniformLocation(objectMaterial.shader->shaderID, iterator->first);
                glUniform1f(floatLocation, iterator->second);
            }
        }

        if (!objectMaterial.shader->uniformVectors.empty()) {
            std::map<std::string, glm::vec3>::iterator iterator;
            for (iterator = objectMaterial.shader->uniformVectors.begin();
            iterator != objectMaterial.shader->uniformVectors.end(); iterator++) {
                GLint vectorLocation = getUniformLocation(objectMaterial.shader->shaderID, iterator->first);
                glUniform3f(vectorLocation, iterator->second.x, iterator->second.y, iterator->second.z);
            }
        }

        glBindVertexArray(objectStaticMesh.vaoID);

        for (GLuint i = 0; i < objectMaterial.shader->attribArrays; i++) {
            glEnableVertexAttribArray(i);
            glBindBuffer(GL_ARRAY_BUFFER, objectStaticMesh.vboIDs.at(i));
            glVertexAttribPointer(i, 3, GL_FLOAT, GL_FALSE, 0, (void*) nullptr);
        };

        if (!objectMaterial.textures.empty()) {
            std::map<GLenum, GLuint>::iterator iterator;
            GLuint i = 0;
            for (iterator = objectMaterial.textures.begin();
            iterator != objectMaterial.textures.end(); iterator++, i++) {
                glActiveTexture(GL_TEXTURE0 + i);
                glBindTexture(iterator->first, iterator->second);
            }
        }
        
        glDrawArrays(GL_TRIANGLES, 0, objectStaticMesh.vertexCount);

        // Cleanup state. TODO; Might need to unbind textures at some point.
        for (GLuint i = 0; i < objectMaterial.shader->attribArrays; i++) {
            glDisableVertexAttribArray(i);
        }
		glBindVertexArray(0);
	}

	static double currFrameDelta = delta;
	if (frames % 120 == 0) currFrameDelta = delta;
    renderText("Frame Time: " + std::to_string(currFrameDelta * 1000).substr(0, 4) + "ms", 0.0f, 0.0f, 0.5f,
            glm::vec3(1.0, 1.0, 1.0));
	int i = 1;
	double deltaSum = 0.0;

	static std::array<double, 10> currDeltas;
	for (const auto &system : Pathfinder::getInstance().getSystems()) {
	    i++;
        std::string name = system->systemName;
        double currDelta = system->systemDelta;

        if (frames % 120 == 0) currDeltas[i - 1] = currDelta;

        deltaSum += currDelta;
        renderText(name + ": " + std::to_string(currDeltas[i - 1] * 1000).substr(0, 4) + "ms (" +
                   std::to_string(currDeltas[i - 1] / currFrameDelta * 100).substr(0, 4) + "%)", 0.0f, 16.0f * i, 0.5f,
                   glm::vec3(1.0, 1.0, 1.0));
	}
	i++;

	static double sleepTime = delta - deltaSum;
	if (frames % 120 == 0) {
	    sleepTime = delta - deltaSum;
	}
	renderText("Sleep: " + std::to_string(sleepTime * 1000).substr(0, 4) + "ms (" +
	    std::to_string(sleepTime / currFrameDelta * 100).substr(0, 4) + "%)", 0.0f, 16.0f * i, 0.5f,
	    glm::vec3(1.0, 1.0, 1.0));
    i += 2;

    renderText("Position: " + glm::to_string(playerTransform.position), 0.0f, 16.0f * i, 0.5f,
            glm::vec3(1.0, 1.0, 1.0));
    i++;
    renderText("Velocity: " + glm::to_string(playerTransform.velocity), 0.0f, 16.0f * i, 0.5f,
            glm::vec3(1.0, 1.0, 1.0));
    i++;
    renderText("Rotation: " + glm::to_string(playerTransform.rotation), 0.0f, 16.0f * i, 0.5f,
            glm::vec3(1.0, 1.0, 1.0));

    glfwSwapBuffers(Pathfinder::getInstance().getWindow());
}

void Renderer::renderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color) {
    glUseProgram(textShader);

    GLint textColorLocation = getUniformLocation(textShader, "textColor");
    glUniform3f(textColorLocation, color.x, color.y, color.z);

    GLint projectionLocation = getUniformLocation(textShader, "projection");
    int width, height;
    glfwGetFramebufferSize(Pathfinder::getInstance().getWindow(), &width, &height);
    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(width), 0.0f, static_cast<GLfloat>(height));
    glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, &projection[0][0]);

    y = (height - 16) - y;
    x += 2;

    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(textVAO);

    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = characters[*c];

        GLfloat xpos = x + ch.bearing.x * scale;
        GLfloat ypos = y - (ch.size.y - ch.bearing.y) * scale;

        GLfloat w = ch.size.x * scale;
        GLfloat h = ch.size.y * scale;

        GLfloat vertices[6][4] = {
                { xpos,     ypos + h,   0.0, 0.0 },
                { xpos,     ypos,       0.0, 1.0 },
                { xpos + w, ypos,       1.0, 1.0 },

                { xpos,     ypos + h,   0.0, 0.0 },
                { xpos + w, ypos,       1.0, 1.0 },
                { xpos + w, ypos + h,   1.0, 0.0 }
        };

        glBindTexture(GL_TEXTURE_2D, ch.textureID);

        glBindBuffer(GL_ARRAY_BUFFER, textVBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDrawArrays(GL_TRIANGLES, 0, 6);

        x += (ch.advance >> 6) * scale;
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

GLint Renderer::getUniformLocation(GLuint shaderID, std::string location) {
    static std::map<GLuint, std::map<std::string, GLint>> shaderMap;
    auto shaderIterator = shaderMap.find(shaderID);
    if (shaderIterator != shaderMap.end()) {
        auto locationMap = shaderIterator->second;
        auto locationIterator = locationMap.find(location);
        if (locationIterator != locationMap.end()) {
            return locationIterator->second;
        } else {
            GLint uniformLocation = glGetUniformLocation(shaderID, location.c_str());
            locationMap.insert(std::make_pair(location, uniformLocation));
            return uniformLocation;
        }
    } else {
        shaderMap.insert(std::make_pair(shaderID, std::map<std::string, GLint>()));
        GLint uniformLocation = glGetUniformLocation(shaderID, location.c_str());
        shaderIterator = shaderMap.find(shaderID);
        shaderIterator->second.insert(std::make_pair(location, uniformLocation));
        return uniformLocation;
    }
}