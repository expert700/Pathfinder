#pragma once

#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <stb_image.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "ECS/System.h"
#include "Util/Logger.h"
#include "../Pathfinder.h"
#include "ShaderRegistry.h"
#include "../GeomGen/PrimitiveGenerator.h"
#include "Component/Camera.h"
#include "Component/Material.h"
#include "ECS/Component/Transform.h"

struct Character {
    GLuint textureID;
    glm::ivec2 size;
    glm::ivec2 bearing;
    GLuint advance;
};


class Renderer: public System {
public:
	void init(entt::DefaultRegistry &registry) override;
	void process(double delta, entt::DefaultRegistry &registry) override;

private:
    ShaderRegistry shaderRegistry;

    entt::DefaultRegistry::entity_type lightSphere;
    entt::DefaultRegistry::entity_type spaceship;
    entt::DefaultRegistry::entity_type spaceship2;

    GLuint textVAO, textVBO, textShader;
    FT_Library ft;
    FT_Face face;
    std::map<GLchar, Character> characters;

    void renderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
    GLint getUniformLocation(GLuint shaderID, std::string location);
};
