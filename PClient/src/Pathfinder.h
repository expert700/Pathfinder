#pragma once

#include <csignal>
#include <chrono>
#include <thread>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Graphics/Renderer.h"
#include "Input/Input.h"
#include "ECS/System/Physics.h"

class Pathfinder {
public:
    static Pathfinder& getInstance();

    void initWindow();
	void initGame();
    void loop();
    void cleanup();

	GLFWwindow* getWindow();
	entt::DefaultRegistry::entity_type getPlayerEntity();
	std::vector<std::shared_ptr<System>> getSystems();
private:
    Pathfinder();
    ~Pathfinder();

    GLFWwindow* window;

    entt::DefaultRegistry registry;
    entt::DefaultRegistry::entity_type playerEntity;

    std::vector<std::shared_ptr<System>> systems;
};
