#pragma once

#include <algorithm>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/string_cast.hpp>

#include "../Pathfinder.h"
#include "ECS/System.h"

class Input: public System {
public:
    void init(entt::DefaultRegistry &registry) override;
    void process(double delta, entt::DefaultRegistry &registry) override;

private:
	const double MOUSE_SPEED = 1.0;
	const double MOVE_SPEED = 2500.0;
};
