#include "Input.h"

void Input::init(entt::DefaultRegistry &registry) {
    glfwSetInputMode(Pathfinder::getInstance().getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Input::process(double delta, entt::DefaultRegistry &registry) {
    systemName = "Input";

	glfwPollEvents();
	int focus = glfwGetWindowAttrib(Pathfinder::getInstance().getWindow(), GLFW_FOCUSED);

	double posX, posY;
	glfwGetCursorPos(Pathfinder::getInstance().getWindow(), &posX, &posY);

	static double oldPosX = posX;
	static double oldPosY = posY;

	static bool cursorLock = true;
	if (cursorLock && focus) {


		auto playerEntity = Pathfinder::getInstance().getPlayerEntity();
		auto& cam = registry.get<Camera>(playerEntity);
		auto& transform = registry.get<Transform>(playerEntity);
        
		glm::mat4 rotationMatrix = glm::toMat4(transform.rotation);
		
		cam.up = glm::normalize(glm::vec3(rotationMatrix * glm::vec4(0.0, 1.0, 0.0, 0.0)));
		cam.right = glm::normalize(glm::vec3(rotationMatrix * glm::vec4(1.0, 0.0, 0.0, 0.0)));
    
		cam.forward = glm::cross(cam.up, cam.right);

		if (posX != oldPosX || posY != oldPosY) {
			double diffX = oldPosX - posX;
			double diffY = oldPosY - posY;

            oldPosX = posX;
            oldPosY = posY;

			transform.rotation *= glm::quat(glm::vec3(0, 1, 0) * (float) diffX * (float) delta * (float) MOUSE_SPEED);
            transform.rotation *= glm::quat(glm::vec3(1, 0, 0) * (float) diffY * (float) delta * (float) MOUSE_SPEED);
		}

		int estate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_E);
		int qstate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_Q);
		int rstate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_R);
		int wstate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_W);
		int astate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_A);
		int sstate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_S);
		int dstate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_D);
        int spacestate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_SPACE);
        int ctrlstate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_LEFT_CONTROL);
        int escstate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_ESCAPE);
        int shiftstate = glfwGetKey(Pathfinder::getInstance().getWindow(), GLFW_KEY_LEFT_SHIFT);
        
		if (estate == GLFW_PRESS) transform.rotation *= glm::quat(glm::vec3(0, 0, -1) * (float)delta * (float)MOUSE_SPEED);
		if (qstate == GLFW_PRESS) transform.rotation *= glm::quat(glm::vec3(0, 0, 1) * (float)delta * (float)MOUSE_SPEED);

		if (rstate == GLFW_PRESS) {
			transform.rotation = glm::quat();
			transform.position = glm::vec3();
		}
		
		double moveSpeedModifier = 1.0;
		if (shiftstate == GLFW_PRESS) moveSpeedModifier = 10.0;

        transform.velocity = glm::vec3();
		auto speed = static_cast<float>(MOVE_SPEED * moveSpeedModifier);
		if (wstate == GLFW_PRESS) transform.velocity += cam.forward * speed;
        if (astate == GLFW_PRESS) transform.velocity -= cam.right * speed;
        if (sstate == GLFW_PRESS) transform.velocity -= cam.forward * speed;
        if (dstate == GLFW_PRESS) transform.velocity += cam.right * speed;
        if (spacestate == GLFW_PRESS) transform.velocity += cam.up * speed;
        if (ctrlstate == GLFW_PRESS) transform.velocity -= cam.up * speed;
        if (escstate == GLFW_PRESS) glfwSetWindowShouldClose(Pathfinder::getInstance().getWindow(), true);
	}

	static int oldState = GLFW_RELEASE;
	int newState = glfwGetMouseButton(Pathfinder::getInstance().getWindow(), GLFW_MOUSE_BUTTON_RIGHT);
	if (newState != oldState) {
		if (oldState == GLFW_PRESS && newState == GLFW_RELEASE) {
			cursorLock = !cursorLock;
			glfwSetInputMode(Pathfinder::getInstance().getWindow(), GLFW_CURSOR, cursorLock ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);

			int width, height;
			glfwGetFramebufferSize(Pathfinder::getInstance().getWindow(), &width, &height);
			double centerX = width / 2.0;
			double centerY = height / 2.0;

			glfwSetCursorPos(Pathfinder::getInstance().getWindow(), centerX, centerY);
			oldPosX = centerX;
			oldPosY = centerY;
		}
		oldState = newState;
	}
}
