#include "Pathfinder.h"
#include "Pathfinder.h"

Pathfinder::Pathfinder() {}

Pathfinder::~Pathfinder() {}


Pathfinder& Pathfinder::getInstance() {
	static Pathfinder instance;
    return instance;
}

void Pathfinder::initWindow() {
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    window = glfwCreateWindow(1280, 720, "Pathfinder", nullptr, nullptr);
    if (window == nullptr) {
        glfwTerminate();
        raise(1);
    }
    
    glfwMakeContextCurrent(window);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
	double centerX = width / 2.0;
	double centerY = height / 2.0;
	// Center cursor before we actually go in and do anything.
	glfwSetCursorPos(window, centerX, centerY);

    glViewport(0, 0, width, height);

	// Who needs vsync when you have 1000 fps!
	glfwSwapInterval(0);
    
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        raise(1);
    }
}

void Pathfinder::initGame() {
    playerEntity = registry.create();


	systems.push_back(std::make_shared<Renderer>());
    systems.push_back(std::make_shared<Input>());
    systems.push_back(std::make_shared<Physics>());

    for (const auto &system : systems) {
        system->init(registry);
    }
}

void Pathfinder::loop() {
	double delta = 0.06f;
    while (!glfwWindowShouldClose(window)) {
		std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

		for(const auto &system : systems) {
		    std::chrono::high_resolution_clock::time_point sysStart = std::chrono::high_resolution_clock::now();
		    system->process(delta, registry);
            std::chrono::high_resolution_clock::time_point sysEnd = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> timespan = std::chrono::duration_cast<std::chrono::duration<double>>(sysEnd - sysStart);
            system->systemDelta = timespan.count();
		}

		std::chrono::high_resolution_clock::time_point midpoint = std::chrono::high_resolution_clock::now();
        auto midpointTimespan = std::chrono::duration_cast<std::chrono::nanoseconds>(midpoint - start);
        long long ns = midpointTimespan.count();
        if (ns < 4000000) {
            std::this_thread::sleep_for(std::chrono::nanoseconds(4000000 - ns));
        }

		std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> timespan = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
		delta = timespan.count();
    }
}

void Pathfinder::cleanup() {
    glfwTerminate();
}

GLFWwindow* Pathfinder::getWindow() {
	return window;
}

entt::DefaultRegistry::entity_type Pathfinder::getPlayerEntity() {
    return playerEntity;
}

std::vector<std::shared_ptr<System>> Pathfinder::getSystems() {
    return systems;
}

int main(int argc, char** argv) {
    Pathfinder& game = Pathfinder::getInstance();

    game.initWindow();
	game.initGame();
    game.loop();
    game.cleanup();

    return 0;
}
