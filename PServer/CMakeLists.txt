cmake_minimum_required(VERSION 3.9)
project(PServer)

file(GLOB_RECURSE SOURCE_FILES "src/*.cpp" "src/*.h")

# Important Intellisense Note:
# When including new things it won't recognize them easily.
# You need to delete Browse.VC.db then regenerate the CMake Cache.
# Alternatively just delete the entire .vs/ folder.
include_directories(${PCore_INCLUDE_DIRS})

add_executable(${PROJECT_NAME} ${SOURCE_FILES})

# I have no idea how this works. It seems too simple.
target_link_libraries(${PROJECT_NAME} PCore)